The files in this directory can be modified and redistributed according
to the terms of General Public License, version 3, from file
../src/COPYING.

This is a program for discrete element modelling of discrete elements.
It appears to be unsuitable for use in problems related to geotechnical
engineering, which is what it was originally conceived for, but, as can
be seen, has good potential for use in driving simulation games.

Type:

./compgraph

to compile the graphical functions and:

./compdemo

to compile the demonstrative programs.

Then:

./demo

or

./triaxial

to run them.

You need GNU/Linux or an equivalent operating system and a version of gcc
with support for OpenMP.
